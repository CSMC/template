<?php

namespace App\Entity\Course;

use App\Entity\Interfaces\ModifiableInterface;
use App\Entity\Traits\ModifiableTrait;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Course\SectionRepository")
 * @ORM\Table(name="section")
 *
 * @Serializer\ExclusionPolicy("all")
 *
 * @Serializer\VirtualProperty(
 *     "course",
 *     exp="object.getCourse().getId()"
 * )
 */
class Section implements ModifiableInterface {
    use ModifiableTrait;
    /**
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     *
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Course", inversedBy="sections")
     * @ORM\JoinColumn(name="course_id", referencedColumnName="id")
     */
    private $course;

    /**
     * @ORM\Column(type="string", length=4, name="number")
     *
     * @Serializer\Expose()
     */
    private $number;

    /**
     * @ORM\ManyToOne(targetEntity="\App\Entity\Misc\Semester")
     * @ORM\JoinColumn(name="semester_id", referencedColumnName="id")
     */
    private $semester;

    /**
     * @ORM\ManyToOne(targetEntity="\App\Entity\User\User")
     * @ORM\JoinColumn(name="instructor_id", referencedColumnName="id")
     *
     * @Serializer\Expose()
     */
    private $instructor;

    /**
     * @ORM\ManyToMany(targetEntity="\App\Entity\User\User")
     * @ORM\JoinTable(name="section_tas",
     * joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     * inverseJoinColumns={@ORM\JoinColumn(name="section_id", referencedColumnName="id")}
     * )
     */
    private $teaching_assistants;

    /**
     * @ORM\ManyToMany(targetEntity="\App\Entity\User\User")
     * @ORM\JoinTable(name="section_students",
     * joinColumns={@ORM\JoinColumn(name="section_id", referencedColumnName="id")},
     * inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     * )
     */
    private $students;

    /**
     * Constructor
     */
    public function __construct() {
        $this->teaching_assistants = new \Doctrine\Common\Collections\ArrayCollection();
        $this->students = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param string $number
     *
     * @return Section
     */
    public function setNumber($number) {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber() {
        return $this->number;
    }

    /**
     * Set course
     *
     * @param \App\Entity\Course\Course $course
     *
     * @return Section
     */
    public function setCourse(\App\Entity\Course\Course $course = null) {
        $this->course = $course;

        return $this;
    }

    /**
     * Get course
     *
     * @return \App\Entity\Course\Course
     */
    public function getCourse() {
        return $this->course;
    }

    /**
     * Set semester
     *
     * @param \App\Entity\Misc\Semester $semester
     *
     * @return Section
     */
    public function setSemester(\App\Entity\Misc\Semester $semester = null) {
        $this->semester = $semester;

        return $this;
    }

    /**
     * Get semester
     *
     * @return \App\Entity\Misc\Semester
     */
    public function getSemester() {
        return $this->semester;
    }

    /**
     * Set instructor
     *
     * @param \App\Entity\User\User $instructor
     *
     * @return Section
     */
    public function setInstructor(\App\Entity\User\User $instructor = null) {
        $this->instructor = $instructor;

        return $this;
    }

    /**
     * Get instructor
     *
     * @return \App\Entity\User\User
     */
    public function getInstructor() {
        return $this->instructor;
    }

    /**
     * Add teachingAssistant
     *
     * @param \App\Entity\User\User $teachingAssistant
     *
     * @return Section
     */
    public function addTeachingAssistant(\App\Entity\User\User $teachingAssistant) {
        $this->teaching_assistants[] = $teachingAssistant;

        return $this;
    }

    /**
     * Remove teachingAssistant
     *
     * @param \App\Entity\User\User $teachingAssistant
     */
    public function removeTeachingAssistant(\App\Entity\User\User $teachingAssistant) {
        $this->teaching_assistants->removeElement($teachingAssistant);
    }

    /**
     * Get teachingAssistants
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTeachingAssistants() {
        return $this->teaching_assistants;
    }

    /**
     * Add student
     *
     * @param \App\Entity\User\User $student
     *
     * @return Section
     */
    public function addStudent(\App\Entity\User\User $student) {
        $this->students[] = $student;

        return $this;
    }

    /**
     * Remove student
     *
     * @param \App\Entity\User\User $student
     */
    public function removeStudent(\App\Entity\User\User $student) {
        $this->students->removeElement($student);
    }

    /**
     * Get students
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStudents() {
        return $this->students;
    }

    public function __toString() {
        return $this->course->getDepartment()
                   ->getAbbreviation() . ' ' . $this->course->getNumber() . '.' . $this->number;
    }
}

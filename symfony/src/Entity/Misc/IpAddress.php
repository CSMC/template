<?php

namespace App\Entity\Misc;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="ip_address")
 */
class IpAddress {
    /**
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\Column(type="bigint", name="address", unique=true)
     */
    private $address;

    /**
     * @ORM\ManyToOne(targetEntity="Room")
     * @ORM\JoinColumn(name="room_id", referencedColumnName="id")
     */
    private $room;

    /**
     * @ORM\OneToMany(targetEntity="Swipe", mappedBy="ip")
     */
    private $swipes;

    /**
     * Set address
     *
     * @param string $address
     *
     * @return IpAddress
     */
    public function setAddress($address) {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress() {
        return long2ip($this->address);
    }

    /**
     * Set room
     *
     * @param \App\Entity\Misc\Room $room
     *
     * @return IpAddress
     */
    public function setRoom(\App\Entity\Misc\Room $room = null) {
        $this->room = $room;

        return $this;
    }

    /**
     * Get room
     *
     * @return \App\Entity\Misc\Room
     */
    public function getRoom() {
        return $this->room;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->swipes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add swipe
     *
     * @param \App\Entity\Misc\Swipe $swipe
     *
     * @return IpAddress
     */
    public function addSwipe(\App\Entity\Misc\Swipe $swipe) {
        $this->swipes[] = $swipe;

        return $this;
    }

    /**
     * Remove swipe
     *
     * @param \App\Entity\Misc\Swipe $swipe
     */
    public function removeSwipe(\App\Entity\Misc\Swipe $swipe) {
        $this->swipes->removeElement($swipe);
    }

    /**
     * Get swipes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSwipes() {
        return $this->swipes;
    }
}

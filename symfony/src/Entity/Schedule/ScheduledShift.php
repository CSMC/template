<?php

namespace App\Entity\Schedule;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Schedule\ScheduledShiftRepository")
 * @ORM\Table(name="scheduled_shift")
 *
 * @Serializer\ExclusionPolicy("all")
 */
class ScheduledShift {
    /**
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     *
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\Column(type="date", name="date_scheduled")
     *
     * @Serializer\Expose()
     * @Serializer\Type("DateTime<'Y-m-d'>")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="Shift")
     * @ORM\JoinColumn(name="shift_id", referencedColumnName="id")
     *
     * @Serializer\Expose()
     */
    private $shift;

    /**
     * @ORM\OneToMany(targetEntity="ShiftAssignment", mappedBy="scheduledShift")
     *
     * @Serializer\Expose()
     */
    private $assignments;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Schedule\Schedule", inversedBy="scheduledShifts")
     */
    private $schedule;

    /**
     * Constructor
     */
    public function __construct() {
        $this->assignments = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return guid
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return ScheduledShift
     */
    public function setDate($date) {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate() {
        return $this->date;
    }

    /**
     * Set shift
     *
     * @param \App\Entity\Schedule\Shift $shift
     *
     * @return ScheduledShift
     */
    public function setShift(\App\Entity\Schedule\Shift $shift = null) {
        $this->shift = $shift;

        return $this;
    }

    /**
     * Get shift
     *
     * @return \App\Entity\Schedule\Shift
     */
    public function getShift() {
        return $this->shift;
    }

    /**
     * Add assignment
     *
     * @param \App\Entity\Schedule\ShiftAssignment $assignment
     *
     * @return ScheduledShift
     */
    public function addAssignment(\App\Entity\Schedule\ShiftAssignment $assignment) {
        $this->assignments[] = $assignment;

        return $this;
    }

    /**
     * Remove assignment
     *
     * @param \App\Entity\Schedule\ShiftAssignment $assignment
     */
    public function removeAssignment(\App\Entity\Schedule\ShiftAssignment $assignment) {
        $this->assignments->removeElement($assignment);
    }

    /**
     * Get assignments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAssignments() {
        return $this->assignments;
    }

    /**
     * Set schedule
     *
     * @param \App\Entity\Schedule\Schedule $schedule
     *
     * @return ScheduledShift
     */
    public function setSchedule(\App\Entity\Schedule\Schedule $schedule = null)
    {
        $this->schedule = $schedule;

        return $this;
    }

    /**
     * Get schedule
     *
     * @return \App\Entity\Schedule\Schedule
     */
    public function getSchedule()
    {
        return $this->schedule;
    }
}

<?php

namespace App\Entity\Schedule;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Schedule\TimesheetRepository")
 * @ORM\Table(name="timesheet")
 */
class Timesheet {
    // TODO this class might be stupid, might just do the shift matching at report time
    /**
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="ScheduledShift")
     * @ORM\JoinTable(name="timesheet_shifts",
     *      joinColumns={@ORM\JoinColumn(name="timesheet_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="scheduled_shift_id", referencedColumnName="id")}
     *      )
     */
    private $shifts;

    /**
     * @ORM\ManyToOne(targetEntity="\App\Entity\User\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\Column(type="datetime", name="time_in", nullable=true)
     */
    private $timeIn;

    /**
     * @ORM\Column(type="datetime", name="time_out", nullable=true)
     */
    private $timeOut;

    /**
     * Constructor
     */
    public function __construct() {
        $this->schedules = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set timeIn
     *
     * @param \DateTime $timeIn
     *
     * @return Timesheet
     */
    public function setTimeIn($timeIn) {
        $this->timeIn = $timeIn;

        return $this;
    }

    /**
     * Get timeIn
     *
     * @return \DateTime
     */
    public function getTimeIn() {
        return $this->timeIn;
    }

    /**
     * Set timeOut
     *
     * @param \DateTime $timeOut
     *
     * @return Timesheet
     */
    public function setTimeOut($timeOut) {
        $this->timeOut = $timeOut;

        return $this;
    }

    /**
     * Get timeOut
     *
     * @return \DateTime
     */
    public function getTimeOut() {
        return $this->timeOut;
    }

    /**
     * Set user
     *
     * @param \App\Entity\User\User $user
     *
     * @return Timesheet
     */
    public function setUser(\App\Entity\User\User $user = null) {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \App\Entity\User\User
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Add shift
     *
     * @param \App\Entity\Schedule\Shift $shift
     *
     * @return Timesheet
     */
    public function addShift(\App\Entity\Schedule\Shift $shift) {
        $this->shifts[] = $shift;

        return $this;
    }

    /**
     * Remove shift
     *
     * @param \App\Entity\Schedule\Shift $shift
     */
    public function removeShift(\App\Entity\Schedule\Shift $shift) {
        $this->shifts->removeElement($shift);
    }

    /**
     * Get shifts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getShifts() {
        return $this->shifts;
    }
}

<?php

namespace App\Entity\Session;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Session\ScheduledSessionRepository")
 * @ORM\Table(name="scheduled_session")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 *
 * @Serializer\ExclusionPolicy("all")
 */
abstract class ScheduledSession extends Session {
    /**
     * @ORM\Column(type="integer", name="repeats")
     *
     * @Serializer\Expose()
     */
    private $repeats;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Session\Timeslot", mappedBy="session", cascade={"persist", "remove"})
     *
     * @Serializer\Expose()
     */
    private $timeslots;

    // TODO improve this, probably real inefficient and doesn't work when someone is in more than one section ????
    public function isRegistered($user) {
        foreach ($this->timeslots as $timeslot) {
            foreach ($timeslot->getRegistrations() as $registration) {
                if ($registration->getUser() == $user) {
                    return true;
                }
            }
        }

        return false;
    }

    public function hasAttended($user) {
        foreach ($this->timeslots as $timeslot) {
            foreach ($timeslot->getAttendances() as $attendance) {
                if ($attendance->getUser() == $user) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->timeslots = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add timeslot
     *
     * @param Timeslot $timeslot
     *
     * @return ScheduledSession
     */
    public function addTimeslot(Timeslot $timeslot) {
        $this->timeslots[] = $timeslot;

        return $this;
    }

    /**
     * Remove timeslot
     *
     * @param Timeslot $timeslot
     */
    public function removeTimeslot(Timeslot $timeslot) {
        $this->timeslots->removeElement($timeslot);
    }

    /**
     * Get timeslots
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTimeslots() {
        return $this->timeslots;
    }

    public function getAttendances() {
        $attendances = array();
        foreach ($this->timeslots as $timeslot) {
            $attendances = array_merge($attendances, $timeslot->getAttendances()->toArray());
        }

        return new ArrayCollection($attendances);
    }

    public function getStartDate() {
        $start = (new \DateTime($this->timeslots->first()->getScheduledTime()->format('m/d/Y')))->setTime(0, 0, 0);
        foreach ($this->timeslots as $timeslot) {
            $date = (new \DateTime($timeslot->getScheduledTime()->format('m/d/Y')))->SetTime(0, 0, 0);
            if($date < $start) {
                $start = $date;
            }
        }

        return $start;
    }

    public function getEndDate() {
        $end = (new \DateTime($this->timeslots->first()->getScheduledTime()->format('m/d/Y')))->setTime(0, 0, 0);
        foreach ($this->timeslots as $timeslot) {
            $date = (new \DateTime($timeslot->getScheduledTime()->format('m/d/Y')))->SetTime(0, 0, 0);
            if($date > $end) {
                $end = $date;
            }
        }

        return $end;
    }

    /**
     * Set repeats
     *
     * @param integer $repeats
     *
     * @return ScheduledSession
     */
    public function setRepeats($repeats)
    {
        $this->repeats = $repeats;

        return $this;
    }

    /**
     * Get repeats
     *
     * @return integer
     */
    public function getRepeats()
    {
        return $this->repeats;
    }
}

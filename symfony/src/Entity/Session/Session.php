<?php

namespace App\Entity\Session;

use App\Entity\Traits\ModifiableTrait;
use App\Entity\Interfaces\ModifiableInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="session")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 *
 * @Serializer\ExclusionPolicy("all")
 */
abstract class Session implements ModifiableInterface {
    use ModifiableTrait;
    /**
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     *
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="Request", inversedBy="session")
     * @ORM\JoinColumn(name="request_id", referencedColumnName="id", nullable=true)
     */
    private $request;

    /**
     * @ORM\Column(type="string", length=128, name="topic")
     *
     * @Serializer\Expose()
     */
    private $topic;

    /**
     * @ORM\Column(type="string", length=1024, name="description", nullable=true)
     *
     * @Serializer\Expose()
     */
    private $description;

    /**
     * @ORM\Column(type="string", name="student_instructions", length=1024, nullable=true)
     *
     * @Serializer\Expose()
     */
    private $studentInstructions;

    /**
     * @ORM\Column(type="string", name="mentor_instructions", length=1024, nullable=true)
     *
     * @Serializer\Expose()
     */
    private $mentorInstructions;

    /**
     * @ORM\ManyToMany(targetEntity="\App\Entity\Course\Section")
     * @ORM\JoinTable(name="session_sections",
     *      joinColumns={@ORM\JoinColumn(name="session_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="section_id", referencedColumnName="id")}
     *      )
     *
     * @Serializer\Expose()
     */
    private $sections;

    /**
     * @ORM\Column(type="boolean", name="graded")
     *
     * @Serializer\Expose()
     */
    private $graded;

    /**
     * @ORM\Column(type="boolean", name="numeric_grade")
     *
     * @Serializer\Expose()
     */
    private $numericGrade;

    /**
     * @ORM\Column(type="boolean", name="active")
     *
     * @Serializer\Expose()
     */
    private $active;

    /**
     * @ORM\ManyToMany(targetEntity="\App\Entity\Misc\File", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="session_files",
     *      joinColumns={@ORM\JoinColumn(name="session_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="file_id", referencedColumnName="id")}
     *      )
     */
    private $files;

    /**
     * @Assert\All(
     *      @Assert\File(
     *          maxSize = "5M",
     *          mimeTypes = {"application/pdf", "application/x-pdf", "application/msword", 
     *              "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
     *              "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
     *              "application/vnd.ms-powerpoint",
     *              "application/vnd.openxmlformats-officedocument.presentationml.presentation",
     *              "application/rtf"},
     *          mimeTypesMessage = "Supported file types: .pdf, .doc, .docx, .xls, .xlsx, .ppt, .pptx, .rtf"
     *      )
     * )
     */
    private $uploadedFiles;

    /**
     * Constructor
     */
    public function __construct() {
        $this->sections = new ArrayCollection();
        $this->files = new ArrayCollection();
    }

    // could be useful
    abstract public function getStartDate();

    abstract public function getEndDate();

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set request
     *
     * @param integer $request
     *
     * @return Session
     */
    public function setRequest($request) {
        $this->request = $request;

        return $this;
    }

    /**
     * Get request
     *
     * @return integer
     */
    public function getRequest() {
        return $this->request;
    }

    /**
     * Set topic
     *
     * @param string $topic
     *
     * @return Session
     */
    public function setTopic($topic) {
        $this->topic = $topic;

        return $this;
    }

    /**
     * Get topic
     *
     * @return string
     */
    public function getTopic() {
        return $this->topic;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Session
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set studentInstructions
     *
     * @param string $studentInstructions
     *
     * @return Session
     */
    public function setStudentInstructions($studentInstructions) {
        $this->studentInstructions = $studentInstructions;

        return $this;
    }

    /**
     * Get studentInstructions
     *
     * @return string
     */
    public function getStudentInstructions() {
        return $this->studentInstructions;
    }

    /**
     * Set mentorInstructions
     *
     * @param string $mentorInstructions
     *
     * @return Session
     */
    public function setMentorInstructions($mentorInstructions) {
        $this->mentorInstructions = $mentorInstructions;

        return $this;
    }

    /**
     * Get mentorInstructions
     *
     * @return string
     */
    public function getMentorInstructions() {
        return $this->mentorInstructions;
    }

    /**
     * Add sections
     *
     * @param \App\Entity\Course\Section $sections
     *
     * @return Session
     */
    public function addSection(\App\Entity\Course\Section $sections) {
        $this->sections [] = $sections;

        return $this;
    }

    /**
     * Remove sections
     *
     * @param \App\Entity\Course\Section $sections
     */
    public function removeSection(\App\Entity\Course\Section $sections) {
        $this->sections->removeElement($sections);
    }

    /**
     * Get sections
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSections() {
        return $this->sections;
    }

    /**
     * Add files
     *
     * @param \App\Entity\Misc\File $files
     *
     * @return Session
     */
    public function addFile(\App\Entity\Misc\File $files) {
        $this->files [] = $files;

        return $this;
    }

    /**
     * Remove files
     *
     * @param \App\Entity\Misc\File $files
     */
    public function removeFile(\App\Entity\Misc\File $files) {
        $this->files->removeElement($files);
    }

    public function getUploadedFiles() {
        return $this->uploadedFiles;
    }

    public function setUploadedFiles($files) {
        $this->uploadedFiles = $files;
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFiles() {
        return $this->files;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Session
     */
    public function setActive($active) {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function isActive() {
        return $this->active;
    }

    /**
     * Set graded
     *
     * @param boolean $graded
     *
     * @return Session
     */
    public function setGraded($graded) {
        $this->graded = $graded;

        return $this;
    }

    /**
     * Get graded
     *
     * @return boolean
     */
    public function isGraded() {
        return $this->graded;
    }

    /**
     * Set numericGrade
     *
     * @param boolean $numericGrade
     *
     * @return Session
     */
    public function setNumericGrade($numericGrade) {
        $this->numericGrade = $numericGrade;

        return $this;
    }

    /**
     * Get numericGrade
     *
     * @return boolean
     */
    public function isNumericGrade() {
        return $this->numericGrade;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive() {
        return $this->active;
    }

    public function getInstructors() {
        $instructors = new ArrayCollection();
        foreach ($this->sections as $section) {
            $instructor = $section->getInstructor();
            if (!$instructors->contains($instructor)) {
                $instructors->add($instructor);
            }
        }

        return $instructors;
    }

    /**
     * Get graded
     *
     * @return boolean
     */
    public function getGraded() {
        return $this->graded;
    }

    /**
     * Get numericGrade
     *
     * @return boolean
     */
    public function getNumericGrade() {
        return $this->numericGrade;
    }
}

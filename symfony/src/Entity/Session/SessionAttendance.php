<?php

namespace App\Entity\Session;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="session_attendance")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 */
class SessionAttendance extends Attendance {

    /**
     * @ORM\Column(type="integer", name="grade", nullable=true)
     */
    private $grade;

    /**
     * @ORM\Column(type="string", name="comments", length=128, nullable=true)
     */
    private $comments;

    /**
     * Set grade
     *
     * @param integer $grade
     *
     * @return SessionAttendance
     */
    public function setGrade($grade) {
        $this->grade = $grade;

        return $this;
    }

    /**
     * Get grade
     *
     * @return integer
     */
    public function getGrade() {
        return $this->grade;
    }

    /**
     * Set comments
     *
     * @param string $comments
     *
     * @return SessionAttendance
     */
    public function setComments($comments) {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string
     */
    public function getComments() {
        return $this->comments;
    }
}

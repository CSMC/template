<?php

namespace App\Entity\Session;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Session\TimeslotRepository")
 * @ORM\Table(name="timeslot")
 *
 * @Serializer\ExclusionPolicy("all")
 *
 * @Serializer\VirtualProperty(
 *     "session",
 *     exp="object.getSession().getId()"
 * )
 */
class Timeslot {
    /**
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     *
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="ScheduledSession", inversedBy="timeslots")
     * @ORM\JoinColumn(name="session_id", referencedColumnName="id")
     */
    private $session;

    /**
     * @ORM\OneToMany(targetEntity="\App\Entity\Schedule\ShiftAssignment", mappedBy="session", cascade={"persist", "detach"})
     *
     * @Serializer\Expose()
     */
    private $assignments;

    /**
     * @ORM\ManyToOne(targetEntity="\App\Entity\Misc\Room")
     * @ORM\JoinColumn(name="room_id", referencedColumnName="id")
     *
     * @Serializer\Expose()
     */
    private $room;

    /**
     * @ORM\Column(type="integer", name="capacity")
     *
     * @Serializer\Expose()
     */
    private $capacity;

    /**
     * @ORM\Column(type="datetime", name="scheduled_time")
     *
     * @Serializer\Expose()
     * @Serializer\Type("DateTime<'Y-m-d H:i'>")
     */
    private $scheduledTime;

//    /**
//     * @ORM\Column(type="time", name="duration")
//     */
//    private $duration;

    // TODO: change the below 2 fields to be swipes

    /**
     * @ORM\Column(type="time", name="start_time", nullable=true)
     */
    private $startTime;

    /**
     * @ORM\Column(type="time", name="end_time", nullable=true)
     */
    private $endTime;

    /**
     * @ORM\ManyToOne(targetEntity="\App\Entity\User\User")
     * @ORM\JoinColumn(name="sub_user_id", referencedColumnName="id")
     */
    private $subMentor;

    /**
     * @ORM\OneToMany(targetEntity="Registration", mappedBy="timeslot", cascade={"remove"})
     *
     * @Serializer\Expose()
     */
    private $registrations;

    /**
     * @ORM\OneToMany(targetEntity="ScheduledSessionAttendance", mappedBy="timeslot", cascade={"remove"})
     */
    private $attendances;

    public function isRegistered($user) {
        foreach ($this->getRegistrations() as $registration) {
            if ($registration->getUser()->getId() == $user->getId()) {
                return true;
            }
        }
        return false;
    }

    public function hasAttended($user) {
        foreach ($this->getAttendances() as $attendance) {
            if ($attendance->getUser()->getId() == $user->getId()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get scheduledTime + duration
     *
     * @return \DateTime
     */
    public function getScheduledEndTime() {
        return (new \DateTime($this->scheduledTime->format('Y/m/d g:i A')))->add($this->getDuration());
    }

    /**
     * Get duration
     *
     * @return \dateinterval
     */
    public function getDuration() {
        return \DateInterval::createFromDateString('90 minutes');
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->assignments = new \Doctrine\Common\Collections\ArrayCollection();
        $this->registrations = new \Doctrine\Common\Collections\ArrayCollection();
        $this->attendances = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return guid
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set capacity
     *
     * @param integer $capacity
     *
     * @return Timeslot
     */
    public function setCapacity($capacity) {
        $this->capacity = $capacity;

        return $this;
    }

    /**
     * Get capacity
     *
     * @return integer
     */
    public function getCapacity() {
        return $this->capacity;
    }

    /**
     * Set scheduledTime
     *
     * @param \DateTime $scheduledTime
     *
     * @return Timeslot
     */
    public function setScheduledTime($scheduledTime) {
        $this->scheduledTime = $scheduledTime;

        return $this;
    }

    /**
     * Get scheduledTime
     *
     * @return \DateTime
     */
    public function getScheduledTime() {
        return $this->scheduledTime;
    }

    /**
     * Set startTime
     *
     * @param \DateTime $startTime
     *
     * @return Timeslot
     */
    public function setStartTime($startTime) {
        $this->startTime = $startTime;

        return $this;
    }

    /**
     * Get startTime
     *
     * @return \DateTime
     */
    public function getStartTime() {
        return $this->startTime;
    }

    /**
     * Set endTime
     *
     * @param \DateTime $endTime
     *
     * @return Timeslot
     */
    public function setEndTime($endTime) {
        $this->endTime = $endTime;

        return $this;
    }

    /**
     * Get endTime
     *
     * @return \DateTime
     */
    public function getEndTime() {
        return $this->endTime;
    }

    /**
     * Set session
     *
     * @param \App\Entity\Session\ScheduledSession $session
     *
     * @return Timeslot
     */
    public function setSession(\App\Entity\Session\ScheduledSession $session = null) {
        $this->session = $session;

        return $this;
    }

    /**
     * Get session
     *
     * @return \App\Entity\Session\ScheduledSession
     */
    public function getSession() {
        return $this->session;
    }

    /**
     * Add assignment
     *
     * @param \App\Entity\Schedule\ShiftAssignment $assignment
     *
     * @return Timeslot
     */
    public function addAssignment(\App\Entity\Schedule\ShiftAssignment $assignment) {
        $this->assignments[] = $assignment;

        return $this;
    }

    /**
     * Remove assignment
     *
     * @param \App\Entity\Schedule\ShiftAssignment $assignment
     */
    public function removeAssignment(\App\Entity\Schedule\ShiftAssignment $assignment) {
        $this->assignments->removeElement($assignment);
    }

    /**
     * Get assignments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAssignments() {
        return $this->assignments;
    }

    /**
     * Set room
     *
     * @param \App\Entity\Misc\Room $room
     *
     * @return Timeslot
     */
    public function setRoom(\App\Entity\Misc\Room $room = null) {
        $this->room = $room;

        return $this;
    }

    /**
     * Get room
     *
     * @return \App\Entity\Misc\Room
     */
    public function getRoom() {
        return $this->room;
    }

    /**
     * Set subMentor
     *
     * @param \App\Entity\User\User $subMentor
     *
     * @return Timeslot
     */
    public function setSubMentor(\App\Entity\User\User $subMentor = null) {
        $this->subMentor = $subMentor;

        return $this;
    }

    /**
     * Get subMentor
     *
     * @return \App\Entity\User\User
     */
    public function getSubMentor() {
        return $this->subMentor;
    }

    /**
     * Add registration
     *
     * @param \App\Entity\Session\Registration $registration
     *
     * @return Timeslot
     */
    public function addRegistration(\App\Entity\Session\Registration $registration) {
        $this->registrations[] = $registration;

        return $this;
    }

    /**
     * Remove registration
     *
     * @param \App\Entity\Session\Registration $registration
     */
    public function removeRegistration(\App\Entity\Session\Registration $registration) {
        $this->registrations->removeElement($registration);
    }

    /**
     * Get registrations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRegistrations() {
        return $this->registrations;
    }

    /**
     * Add attendance
     *
     * @param \App\Entity\Session\ScheduledSessionAttendance $attendance
     *
     * @return Timeslot
     */
    public function addAttendance(\App\Entity\Session\ScheduledSessionAttendance $attendance) {
        $this->attendances[] = $attendance;

        return $this;
    }

    /**
     * Remove attendance
     *
     * @param \App\Entity\Session\ScheduledSessionAttendance $attendance
     */
    public function removeAttendance(\App\Entity\Session\ScheduledSessionAttendance $attendance) {
        $this->attendances->removeElement($attendance);
    }

    /**
     * Get attendances
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAttendances() {
        return $this->attendances;
    }

    /**
     * @return mixed
     */
    public function getRemainingSeats() {
        return $this->capacity - (($this->registrations->count() > $this->attendances->count()) ? $this->registrations->count() : $this->attendances->count());
    }
}

<?php

namespace App\Entity\User;

use App\Entity\User\Info\Info;
use App\Entity\User\Role;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\User\UserRepository")
 * @ORM\Table(name="user", uniqueConstraints={@ORM\UniqueConstraint(name="card_idx", columns={"fac", "card_id"})})
 *
 * @Serializer\ExclusionPolicy("all")
 */
class User implements UserInterface, \Serializable {
    /**
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     *
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\Column(type="string", name="first_name", length=32, nullable=true)
     *
     * @Serializer\Expose()
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", name="last_name", length=64, nullable=true)
     *
     * @Serializer\Expose()
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", name="netid", length=9, unique=true)
     *
     * @Serializer\Expose()
     */
    private $username;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User\Info\Info", mappedBy="user")
     *
     * @Serializer\Expose()
     */
    private $info;

    /**
     * @ORM\Column(type="string", name="scancode", length=16, unique=true, nullable=true)
     */
    private $scancode;

    /**
     * @ORM\Column(type="binary", name="fac", length=32, nullable=true, options={"comment":"Facility Access Code"})
     */
    private $fac;

    /**
     * @ORM\Column(type="binary", name="card_id", length=32, nullable=true, options={"comment":"Card ID as given by scanner"})
     */
    private $cardId;

    /**
     * @ORM\ManyToMany(targetEntity="Role", inversedBy="users")
     * @ORM\JoinTable(name="user_roles")
     *
     * @Serializer\Expose()
     */
    private $roles;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Misc\Visit", mappedBy="user", cascade={"persist"})
     */
    private $visits;

    /**
     * Constructor
     */
    public function __construct() {
        $this->roles = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName) {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName() {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName) {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName() {
        return $this->lastName;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return User
     */
    public function setUsername($username) {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername() {
        return $this->username;
    }

    /**
     * Set scancode
     *
     * @param string $scancode
     *
     * @return User
     */
    public function setScancode($scancode) {
        $this->scancode = $scancode;

        return $this;
    }

    /**
     * Get scancode
     *
     * @return string
     */
    public function getScancode() {
        return $this->scancode;
    }

    /**
     * Set fac
     *
     * @param string $fac
     *
     * @return User
     */
    public function setFac($fac) {
        $this->fac = $fac ? hex2bin($fac) : null;

        return $this;
    }

    /**
     * Get fac
     *
     * @return string
     */
    public function getFac() {
        return $this->fac ? bin2hex(stream_get_contents($this->fac)) : null;
    }

    /**
     * Set cardId
     *
     * @param string $cardId
     *
     * @return User
     */
    public function setCardId($cardId) {
        $this->cardId = $cardId ? hex2bin($cardId) : null;

        return $this;
    }

    /**
     * Get cardId
     *
     * @return string
     */
    public function getCardId() {
        return $this->cardId ? bin2hex(stream_get_contents($this->cardId)) : null;
    }

    /**
     * Add role
     *
     * @param \App\Entity\User\Role $role
     *
     * @return User
     */
    public function addRole(\App\Entity\User\Role $role) {
        $this->roles [] = $role;

        return $this;
    }

    /**
     * Remove role
     *
     * @param \App\Entity\User\Role $role
     */
    public function removeRole(\App\Entity\User\Role $role) {
        $this->roles->removeElement($role);
    }

    /**
     * Get roles
     *
     * Returns role strings
     *
     * @return Role[]
     */
    public function getRoles() {
        return $this->roles->toArray();
    }

    /**
     * Get user roles
     *
     * @return
     */
    public function getUserRoles() {
        return $this->roles;
    }

    /**
     * Check if user has role
     *
     * @param Role $role
     *
     * @return boolean
     */
    public function hasRole($role) {
        foreach ($this->roles as $role) {
            if ($role->getName() == $role) {
                return true;
            }
        }
        return false;
    }

    /**
     * Set info
     *
     * @param \App\Entity\User\Info\Info $info
     *
     * @return User
     */
    public function setInfo(\App\Entity\User\Info\Info $info = null) {
        $this->info = $info;

        return $this;
    }

    /**
     * Get info
     *
     * @return \App\Entity\User\Info\Info
     */
    public function getInfo() {
        return $this->info;
    }


    /**
     * Add visit
     *
     * @param \App\Entity\Misc\Visit $visit
     *
     * @return User
     */
    public function addVisit(\App\Entity\Misc\Visit $visit) {
        $this->visits[] = $visit;

        return $this;
    }

    /**
     * Remove visit
     *
     * @param \App\Entity\Misc\Visit $visit
     */
    public function removeVisit(\App\Entity\Misc\Visit $visit) {
        $this->visits->removeElement($visit);
    }

    /**
     * Get visits
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVisits() {
        return $this->visits;
    }

    public function getEmail() {
        return ($this->info && $this->info->getEmail())
            ? $this->info->getEmail()
            : ($this->username . '@utdallas.edu');
    }

    public function __toString() {
        return $this->getFirstName() . ' ' . $this->getLastName();
    }

    public function serialize() {
        return json_encode(
            array(
                $this->id,
                $this->firstName,
                $this->lastName,
                $this->username,
                $this->scancode,
                $this->roles
            ));
    }

    public function unserialize($serialized) {
        list ($this->id, $this->firstName, $this->lastName, $this->username, $this->scancode, $this->roles) = json_decode(
            $serialized);
    }

    /**
     * Unused
     */
    public function getPassword() {
        return null;
    }

    /**
     * Unused
     *
     * Must return null
     */
    public function getSalt() {
        return null;
    }

    /**
     * Unused
     *
     * Must return null
     */
    public function eraseCredentials() {
        return null;
    }
}

var Encore = require('@symfony/webpack-encore');

Encore
    // the project directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // the public path used by the web server to access the previous directory
    .setPublicPath('/build')
    // .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning()

    .addEntry('js/main', './assets/js/main.js')
    .addStyleEntry('css/global', './assets/css/global.scss')

    .createSharedEntry('js/vendor', [
        'jquery',
        'bootstrap-sass/assets/javascripts/bootstrap.js'
    ])

    .enableSassLoader()
    .autoProvidejQuery()
;

module.exports = Encore.getWebpackConfig();
